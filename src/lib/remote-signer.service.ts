import { Injectable } from '@angular/core';
import { SignerService, SignerServiceConfiguration, RememberVerificationCode, RequestVerificationCode } from './signer.service';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, ConfigurationService, TemplatePipe } from '@universis/common';
import { BsModalService } from 'ngx-bootstrap';

@Injectable()
export class RemoteSignerService extends SignerService implements RememberVerificationCode, RequestVerificationCode {
    
    constructor(context: AngularDataContext,
        http: HttpClient,
        errorService: ErrorService,
        translateService: TranslateService,
        configurationService: ConfigurationService,
        modalService: BsModalService,
        template: TemplatePipe) {
        super(context, http, errorService, translateService, configurationService, modalService, template);
        this.rememberVerificationCode = false;
    }

    public getSignerUrl(): string {
        const signerConfiguration = this.configurationService.config.settings.signer as SignerServiceConfiguration;
        if (signerConfiguration == null) {
            throw new Error('Signer configuration is not set');
        }
        if (signerConfiguration.service == null) {
            throw new Error('Remote signer service is not set');
        }
        return this.configurationService.config.settings.signer.service;
    }

    public async requestVerificationCode(): Promise<void> {
        const schema = await this.getSchema();
        const requestCodeEndpoint = schema.paths['/requestCode'];
        if (requestCodeEndpoint != null) {
            const requestCodeUrl = new URL('requestCode', this.getSignerUrl()).toString()   ;
            return this.http.post(requestCodeUrl, null, {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': this.authorization
                },
                responseType: 'json'
            }).toPromise().then((result: any) => {
                const success = result && (result.Outcome === 0 || result.Outcome === 1);
                if (success) {
                    return void 0;
                }
                return Promise.reject(new Error('An error occurred while requesting verification code'));
            });
        }
    }

    async requiresVerificationCode() {
        return true
    }

}