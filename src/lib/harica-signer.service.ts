import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ResponseError } from '@themost/client';
import { ConfigurationService, ErrorService, TemplatePipe } from '@universis/common';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BehaviorSubject, Observable } from 'rxjs';
import { SignerService, Certificate, VerifySignatureResult } from './signer.service';
import { blobToBase64String, base64StringToBlob } from 'blob-util';
import { MD5 } from 'crypto-js';

declare interface HaricaSignerServiceConfiguration {
    service: string;
    verificationUrl?: string;
    embedSignatureUrl?: string;
    signatureInspectorUrl?: string;
}

declare interface HaricaGetCertificatesResponse {
    Issuer: string;
    Subject: string;
    NotBefore: string;
    NotAfter: string;
    SerialNumber: string;
    Thumbprint: string;
    Certificates: string[];
    Success?: boolean;
    Message?: string;
}

declare interface HaricaSignRequest {
    Username: string;
    Password: string;
    SignPassword: string;
    Reason?: string;
    Title?: string;
    FileType: string;
    FileData: string;
    Page?: number;
    Width?: number;
    Height?: number;
    X?: number;
    Y?: number;
    Appearance?: number;
    GraphicalSignature?: string;
    SigFieldName?: string;
}

declare interface HaricaSignResponse {
    Success: boolean;
    Data?: {
        SignedFileData: string;
    };
    ErrData?: {
        Message: string;
        InnerCode?: number;
        Code?: number;
        Module?: string;
    };
}

const GRAPHICAL_SIGNATURE_TYPES = [
    'image/jpg',
    'image/jpeg',
    'image/bpm'
];

class UsernameAndPassword {
    constructor(private value: string) {
        //
    }
    get username(): string {
        const decrypted = atob(this.value.replace(/^Basic\s/g, ''));
        const separator = decrypted.indexOf(':');
        return decrypted.substr(0, separator);
    }

    get password(): string {
        const decrypted = atob(this.value.replace(/^Basic\s/g, ''));
        const separator = decrypted.indexOf(':');
        return decrypted.substr(separator + 1);
    }

    static create(username: string, password: string) {
        return 'Basic ' + btoa(`${username}:${password}`);
    }

}

@Injectable()
export class HaricaSignerService extends SignerService {
    private options: HaricaSignerServiceConfiguration = {
        service: 'https://rsign-api.harica.gr/'
    };
    private _basicAuthorization: string;
    constructor(context: AngularDataContext,
        http: HttpClient,
        errorService: ErrorService,
        translateService: TranslateService,
        configurationService: ConfigurationService,
        modalService: BsModalService,
        template: TemplatePipe) {
        super(context, http, errorService, translateService, configurationService, modalService, template);
        const config: any = this.configurationService.config;
        if (config && config.settings && config.settings.harica) {
            Object.assign(this.options, this.configurationService.config.settings.harica);
        }
    }

    public get status(): BehaviorSubject<any> {
        setTimeout(() => {
            // call signer service and return value
        return this.http.options(new URL('/dsa/v1/Certificates', this.options.service).toString(), {
            observe: 'response'
        }).subscribe((response: HttpResponse<any>) => {
                this.serviceStatus.next({
                    ok: response.ok,
                    status: response.status,
                    statusText: response.statusText
                });
            }, (err: HttpErrorResponse) => {
                this.serviceStatus.next({
                    ok: err.ok,
                    status: err.status,
                    statusText: err.statusText
                });
            });
        }, 500);
        return this.serviceStatus;
      }

    public queryStatus(): Observable<any> {
        return new Observable(subscriber => {
            return this.http.options(new URL('/dsa/v1/Certificates', this.options.service).toString(), {
                observe: 'response'
            }).subscribe((response: HttpResponse<any>) => {
                const result = {
                    ok: response.ok,
                    status: response.status,
                    statusText: response.statusText
                };
                subscriber.next(result);
            }, (err: HttpErrorResponse) => {
                const result = {
                    ok: err.ok,
                    status: err.status,
                    statusText: err.statusText
                };
                subscriber.next(result);
            });
        });
    }

    public async authenticate({ username, password, rememberMe }:
        { username: string; password: string; rememberMe?: boolean; }): Promise<void> {
        const basicAuthorization = UsernameAndPassword.create(username, password);
        // clear cached certificates
        sessionStorage.removeItem('signer.certificates');
        const certificatesResponse = await this.http.post<HaricaGetCertificatesResponse>(
            new URL('/dsa/v1/Certificates', this.options.service).toString(), {
                Username: username,
                Password: password
            }, {
            headers: {
                'Content-Type': 'application/json'
            },
            responseType: 'json',
        }).toPromise();
        if (certificatesResponse && certificatesResponse.Success === false) {
            return Promise.reject(new ResponseError(certificatesResponse.Message, 500));
        }
        if (rememberMe) {
            // set item to session storage
            sessionStorage.setItem('signer.auth', basicAuthorization);
        } else {
            // otherwise use memory
            this._basicAuthorization = basicAuthorization;
        }
    }

    public get authorization(): string {
        return sessionStorage.getItem('signer.auth') || this._basicAuthorization;
    }

    private getCommonName(subject: string): string {
        const matches = /CN=(.*?)(,|$)/g.exec(subject);
        if (matches) {
            return matches[1];
        }
    }

    private getSerialNumber(subject: string): number {
        const matches = /SERIALNUMBER=(\d+)(,|$)/g.exec(subject);
        if (matches) {
            return parseInt(matches[1], 10);
        }
    }

    public async getCertificates(): Promise<any> {
        if (this.authorization == null) {
            return Promise.reject(new ResponseError('Unauthorized', 401.5));
        }
        // cache certificates for this session: disabled
        // const certs = sessionStorage.getItem('signer.certificates');
        // if (certs != null) {
        //     return JSON.parse(certs);
        // }
        const usernameAndPassword = new UsernameAndPassword(this.authorization);
        const result = await this.http.post<HaricaGetCertificatesResponse>(
            new URL('/dsa/v1/Certificates', this.options.service).toString(), {
                Username: usernameAndPassword.username,
                Password: usernameAndPassword.password
            }, {
            headers: {
                'Content-Type': 'application/json'
            },
            responseType: 'json',
        }).toPromise();
        if (result && result.Success === false) {
            return Promise.reject(new ResponseError(result.Message, 500));
        }
        if (result != null) {
            const notAfter = new Date(result.NotAfter);
            const notBefore = new Date(result.NotBefore);
            const now = new Date();
            const finalResult = [
                <Certificate>{
                    commonName: this.getCommonName(result.Subject),
                    issuerDN: result.Issuer,
                    subjectDN: result.Subject,
                    notAfter: result.NotAfter,
                    notBefore: result.NotBefore,
                    serialNumber: this.getSerialNumber(result.Subject),
                    thumbprint: result.Thumbprint,
                    version: 3,
                    sigAlgName: 'SHA256 RSA',
                    sigAlgOID: '1.2.840.113549.1.1.11',
                    expired: (now < notBefore) || (now > notAfter)
                }
            ];
            // cache certificates for this session: disabled
            // sessionStorage.setItem('signer.certificates', JSON.stringify(finalResult));

            // and return result
            return finalResult;
        }
        return [];

    }

    async requiresUsernamePassword(): Promise<boolean> {
        return true;
    }

    async requiresVerificationCode(): Promise<boolean> {
        return true;
    }

    getInspectorUrl() {
        const path = this.options.signatureInspectorUrl || 'signer/signature/inspect';
        return new URL(path, this.context.getBase()).toString();
    }

    getEmbedSignatureLineUrl(): string {
        const path = this.options.embedSignatureUrl || 'signer/signature/embed';
        return new URL(path, this.context.getBase()).toString();
    }

    async syncSignatureGraphic(image: File): Promise<string> {
        if (image == null) {
            return;
        }
        if (GRAPHICAL_SIGNATURE_TYPES.indexOf(image.type) < 0) {
            throw new Error('Invalid graphical signature type. Expected image/jpg or image/bpm');
        }
        const imageName = 'UNIVERSIS1';
        const usernameAndPassword = new UsernameAndPassword(this.authorization);
        // get image MD5 hash
        let hash = localStorage.getItem('user.signatureGraphic.hash');
        // get image hash
        const base64image = await blobToBase64String(image);
        let shouldUpdate = false;
        if (hash == null) {
            // calculate hash
            hash = MD5(base64image).toString();
            shouldUpdate = true;
        } else {
            const newHash = MD5(base64image).toString();
            if (hash !== newHash) {
                // set new hash
                hash = newHash;
                // and prepare to update image
                shouldUpdate = true;
            }
        }
        if (shouldUpdate) {
            // upload image
            await this.http.post(new URL('/dsa/v1/GraphicalSignature', this.options.service).toString(), {
                headers: {
                    'Content-Type': 'application/json'
                },
                body: {
                    Username: usernameAndPassword.username,
                    Password: usernameAndPassword.password,
                    Name: imageName,
                    Image: base64image
                },
                responseType: 'json',
            }).toPromise();
            // hold hash for future validation
            localStorage.setItem('user.signatureGraphic.hash', hash);
        }
        return imageName;
    }

    public async sign(form: FormData): Promise<any> {
        // prepare message
        const usernameAndPassword = new UsernameAndPassword(this.authorization);
        // get file
        const file = form.get('file') as File;
        // get file extension
        let fileType: string;
        if (file.name) {
            const matches = /\.([a-zA-Z0-9]+)$/g.exec(file.name);
            if (matches) {
                fileType = matches[1].toLowerCase();
            }
        }
        const image = form.get('image') as File;
        const graphicalSignature = await this.syncSignatureGraphic(image);

        let fileData: string;
        const extraHeaders = this.context.getService().getHeaders();
        // force embed signature line (office OpenXML documents)
        if (file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            const embedSignatureFormData = new FormData();
            // set file.
            embedSignatureFormData.append('file', file, file.name);
            // verify signature(s).
            const embedResponse = await this.http.post(this.getEmbedSignatureLineUrl(), embedSignatureFormData, {
                headers: extraHeaders,
                responseType: 'blob',
                observe: 'response'
            }).toPromise();
            // get updated file as base64 string
            fileData = await blobToBase64String(embedResponse.body);
        } else {
            fileData = await blobToBase64String(file);
        }
        // create message
        const message: HaricaSignRequest = {
            Username: usernameAndPassword.username,
            Password: usernameAndPassword.password,
            SignPassword: form.has('otp') ? form.get('otp').toString() : null,
            FileType: fileType,
            FileData: fileData
        };
        if (graphicalSignature != null) {
            message.GraphicalSignature = graphicalSignature;
        }
        // add position if it is defined
        if (form.has('position') && fileType === 'pdf') {
            const position = form.get('position').toString();
            message.Page = form.has('page') ? parseInt(form.get('page').toString(), 10) : 1;
            const rect = position.split(',');
            if (position !== SignerService.DEFAULT_SIGN_POSITION) {
                // convert rectangle(llx, lly, urx, ury) given by inspector
                // to x, y, width, height dimensions
                const llx = parseInt(rect[0], 10);
                const lly = parseInt(rect[1], 10);
                const urx = Math.round(parseInt(rect[2], 10));
                const ury = Math.round(parseInt(rect[3], 10));
                message.X = llx;
                message.Y = (ury - lly) < 0 ? lly + (ury - lly) : lly; // add bottom margin  (8 pixels)
                message.Width = urx - llx;
                message.Height = Math.abs(ury - lly);
            } else {
                // set default position (do nothing)
                message.X = parseInt(rect[0], 10);
                message.Y = parseInt(rect[1], 10);
                message.Width = parseInt(rect[2], 10);
                message.Height = parseInt(rect[3], 10);
            }
        }
        if (form.has('reason')) {
            message.Reason = form.get('reason').toString();
        }
        if (form.has('name')) {
            message.SigFieldName = form.get('name').toString();
        }
        // set the default endpoint
        let endpoint = '/dsa/v1/sign';
        if (fileType === 'xlsx' || fileType === 'docx') {
            // set default SigFieldName
            message.SigFieldName = '{587CB063-81C4-4369-924F-A5E8C14F93A5}';
            // change endpoint
            endpoint = '/dsa/v1/signSigField';
        }
        // execute request
        const result = await this.http.post<HaricaSignResponse>(new URL(endpoint, this.options.service).toString(), message, {
            headers: {
                'Content-Type': 'application/json'
            },
            responseType: 'json'
        }).toPromise();
        if (result.Success === false) {
            throw new ResponseError(result.ErrData.Message, 500);
        }
        return base64StringToBlob(result.Data.SignedFileData);
    }

    public verifyDocument(file: any): Promise<VerifySignatureResult[]> {
        // create formData.
        const formData = new FormData();
        // set file.
        formData.append('file', file, file.name);
        // verify signature(s).
        const extraHeaders = this.context.getService().getHeaders();
        return this.http.post<VerifySignatureResult[]>(this.getVerificationUrl(), formData, {
          headers: Object.assign({
              'Accept': 'application/json'
          }, extraHeaders),
          responseType: 'json'
        }).toPromise();
      }

    destroy(): void {
        if (this._basicAuthorization) {
            this._basicAuthorization = null;
        }
    }

}
