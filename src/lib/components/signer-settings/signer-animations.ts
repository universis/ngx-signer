import {animate, state, style, transition, trigger} from '@angular/animations';

export const fadeAnimation = trigger('fadeAnimation', [
  state('true', style({ opacity: 1 })),
  state('void', style({ opacity: 0 })),
  transition(':enter', animate('500ms ease-in-out')),
  transition(':leave', animate('500ms ease-in-out'))
]);
