import { formatDate } from '@angular/common';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService, LoadingService, ModalService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import {SignerService, VerifySignatureResult, Certificate} from '../../signer.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'universis-signature-verification',
  templateUrl: './signature-verification.component.html'
})
export class SignatureVerificationComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  @Input() file: any;
  @Input() fileUrl: string;
  @Input() extras: Object;
  @Input() fileExtension: string;

  public loading = true;
  public signatureVerificationResults: VerifySignatureResult[];
  public signatureProperties = [];
  public lastError: any;
  public DNAttributesLocales = [
    {
      attribute: 'commonName',
      locale: 'DistNameAttributes.CommonName'
    },
    {
      attribute: 'organizationalUnit',
      locale: 'DistNameAttributes.OrganizationalUnit'
    },
    {
      attribute: 'organization',
      locale: 'DistNameAttributes.Organization'
    },
    {
      attribute: 'locality',
      locale: 'DistNameAttributes.Locality'
    },
    {
      attribute: 'stateOrProvinceName',
      locale: 'DistNameAttributes.StateOrProvinceName'
    },
    {
      attribute: 'countryName',
      locale: 'DistNameAttributes.CountryName'
    }
  ];
  public certificateAttributeLocales = [
    {
      attribute: 'notBefore',
      locale: 'CertificateAttributeLocales.NotAfter'
    },
    {
      attribute: 'notAfter',
      locale: 'CertificateAttributeLocales.NotBefore'
    },
    {
      attribute: 'expired',
      locale: 'CertificateAttributeLocales.Expired',
      isTranslatable: true
    },
    {
      attribute: 'serialNumber',
      locale: 'CertificateAttributeLocales.SerialNumber'
    },
    {
      attribute: 'sigAlgName',
      locale: 'CertificateAttributeLocales.SigAlgName'
    },
    {
      attribute: 'sigAlgOID',
      locale: 'CertificateAttributeLocales.SigAlgOID'
    },
    {
      attribute: 'thumbprint',
      locale: 'CertificateAttributeLocales.Thumbprint'
    },
    {
      attribute: 'version',
      locale: 'CertificateAttributeLocales.Version'
    },
    {
      attribute: 'signingDate',
      locale: 'CertificateAttributeLocales.SigningDate'
    },
    {
      attribute: 'reason',
      locale: 'CertificateAttributeLocales.Reason'
    }
  ];
  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _signer: SignerService,
    private _context: AngularDataContext,
    private _config: ConfigurationService
  ) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-xl';
    this.okButtonDisabled = false;
    this.modalTitle = 'SignatureVerification';
  }

  async ngOnInit() {
    // show loading
    this._loadingService.showLoading();
    // query signer status
      try {
          // check if file is passed
          if (this.file == null) {
            // if not, prepare it for verify
            this.file = await this.prepareBlob(this.fileUrl);
          }
          if (!this.file.name) {
            Object.assign(this.file, {name: `file.${this.fileExtension || 'xlsx'}`});
          }
          // try to verify signature(s)
          this.signatureVerificationResults = await this._signer.verifyDocument(this.file);
          // extract all certificate(s) attributes, though they may not all be displayed
          this.signatureVerificationResults.forEach((result: VerifySignatureResult) => {
            const certificate: Certificate = result.signatureProperties && result.signatureProperties.signingCertificate;
            if (certificate == null) {
              return;
            }
            // TODO: maybe export those results to an interface later
            this.signatureProperties.push({
              subject: {
                commonName: this._getDNAttribute('CN', certificate.subjectDN),
                organizationalUnit: this._getDNAttribute('OU', certificate.subjectDN),
                organization: this._getDNAttribute('O', certificate.subjectDN),
                locality: this._getDNAttribute('L', certificate.subjectDN),
                stateOrProvinceName: this._getDNAttribute('ST', certificate.subjectDN),
                countryName: this._getDNAttribute('C', certificate.subjectDN)
              },
              issuer: {
                commonName: this._getDNAttribute('CN', certificate.issuerDN),
                organizationalUnit: this._getDNAttribute('OU', certificate.issuerDN),
                organization: this._getDNAttribute('O', certificate.issuerDN),
                locality: this._getDNAttribute('L', certificate.issuerDN),
                stateOrProvinceName: this._getDNAttribute('ST', certificate.issuerDN),
                countryName: this._getDNAttribute('C', certificate.issuerDN)
              },
              notAfter: this._formatDate(certificate.notAfter),
              notBefore: this._formatDate(certificate.notBefore),
              serialNumber: certificate.serialNumber,
              sigAlgName: certificate.sigAlgName,
              sigAlgOID: certificate.sigAlgOID,
              thumbprint: certificate.thumbprint,
              version: certificate.version,
              expired: this._yesNoPseudoFormatter(certificate.expired),
              valid: result.valid,
              signingDate: this._formatDate(result.signatureProperties.signingDate),
              reason: result.signatureProperties.reason
            });
          });
      } catch (err) {
        // set last error
        this.lastError = err;
      } finally {
        // show results
        this._loadingService.hideLoading();
        this.loading = false;
      }
  }

  async ok() {
    // close
    return this.cancel();
  }

  cancel(): Promise<any> {
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  ngOnDestroy() {
  }

  prepareBlob(url: string): Promise<Blob> {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      const serviceHeaders = this._context.getService().getHeaders();
      Object.keys(serviceHeaders).forEach((key) => {
        if (serviceHeaders.hasOwnProperty(key)) {
          headers.set(key, serviceHeaders[key]);
        }
      });
      const attachURL = url.replace(/\\/g, '/').replace('/api', '');
      const fileURL = this._context.getService().resolve(attachURL);
      fetch(fileURL, {
        headers: headers,
        credentials: 'include'
      }).then((response) => {
        return resolve(response.blob());
      }).catch(err => {
        return reject(err);
      });
    });
  }

  private _getDNAttribute(attribute: string, distinguishedName: string) {
    if (attribute == null || distinguishedName == null) {
      return;
    }
    const regex = new RegExp(`${attribute}=(.*?)(,|$)`, 'g');
    const matches = regex.exec(distinguishedName);
    if (matches) {
      return matches[1];
    }
    return null;
  }

  private _formatDate(date: string) {
    if (date == null) {
      return;
    }
    return formatDate(new Date(date),
      'dd-MM-yyyy',
      this._config.currentLocale,
      Intl.DateTimeFormat().resolvedOptions().timeZone);
  }

  private _yesNoPseudoFormatter(value: boolean) {
    if (value == null) {
      return;
    }
    return value ? 'yes' : 'no';
  }
}
