import { TestBed } from '@angular/core/testing';

import { SignActionsService } from './sign-actions.service';

describe('SignerActionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignActionsService = TestBed.get(SignActionsService);
    expect(service).toBeTruthy();
  });
});
