import { ModuleWithProviders, NgModule } from '@angular/core';
import { ErrorService, SharedModule, TemplatePipe } from '@universis/common';
import { AdvancedFormsModule } from '@universis/forms';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { VerificationCodeFormComponent } from './components/verification-code-form/verification-code-form.component';
import { SignerService, SignerServiceConfiguration } from './signer.service';
import { DocumentSignComponent } from './components/document-sign/document-sign.component';
import { CommonModule } from '@angular/common';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { FormioModule } from 'angular-formio';
import { DocumentSignActionComponent } from './components/document-sign-action/document-sign-action.component';
import { ConfigurationService } from '@universis/common';
import { AngularDataContext } from '@themost/angular';
import { HttpClient } from '@angular/common/http';
import { HaricaSignerService } from './harica-signer.service';
import { SIGNER_LOCALES } from './i18n';
import { SignatureVerificationComponent } from './components/signature-verification/signature-verification.component';
import {SignerSettingsComponent} from './components/signer-settings/signer-settings.component';
import { RemoteSignerService } from './remote-signer.service';
import { SignActionsService } from './sign-actions.service';

const SignerServices = [
  {
    "serviceType": "HaricaSignerService",
    "name": "HARICA Cloud Signing"
  },
  {
    "serviceType": "SignerService",
    "name": "Universis Signer Application"
  },
  {
    "serviceType": "RemoteSignerService",
    "name": "Remote Signer Service"
  }
]

export function SignerServiceFactory(context: AngularDataContext,
  http: HttpClient,
  errorService: ErrorService,
  translateService: TranslateService,
  configurationService: ConfigurationService,
  modalService: BsModalService,
  template: TemplatePipe) {
  const settings = configurationService.settings as { signer?: SignerServiceConfiguration; };
  let signerServiceType = 'SignerService';
  if (settings && settings.signer) {
    if (settings.signer.configurable) {
      // get last signer service from session storage
      const str = localStorage.getItem('user.signer');
      let userSignerService: { serviceType: string};
      if (str != null) {
        userSignerService = JSON.parse(str);
      }
      // if service type exists
      if (userSignerService && userSignerService.serviceType) {
        signerServiceType = userSignerService.serviceType;
      }
    } else if (settings.signer.use) {
      signerServiceType = settings.signer.use;
    }
    // find service by name
    const signerService = SignerServices.find((item) => {
      return item.serviceType === signerServiceType;
    });
    if (signerService != null) {
      switch (signerService.serviceType) {
        case 'HaricaSignerService':
          return new HaricaSignerService(context, http, errorService, translateService, configurationService, modalService, template);
        case 'SignerService':
          return new SignerService(context, http, errorService, translateService, configurationService, modalService, template);
        case 'RemoteSignerService':
          return new RemoteSignerService(context, http, errorService, translateService, configurationService, modalService, template);
        default:
          console.log('WARN', 'The specified signer service is not yet implemented. Application will use fallback signer service instead.');
          break;
      }
    }
  }
  // use fallback, an instance of SignerService
  return new SignerService(context, http, errorService, translateService, configurationService, modalService, template);
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ModalModule,
    FormioModule,
    AdvancedFormsModule,
    TranslateModule,
    NgxDropzoneModule
  ],
  declarations: [
    VerificationCodeFormComponent,
    DocumentSignComponent,
    DocumentSignActionComponent,
    SignatureVerificationComponent,
    SignerSettingsComponent
  ],
  exports: [
    VerificationCodeFormComponent,
    DocumentSignComponent,
    DocumentSignActionComponent,
    SignatureVerificationComponent,
    SignerSettingsComponent
  ],
  entryComponents: [
    VerificationCodeFormComponent,
    DocumentSignComponent,
    DocumentSignActionComponent,
    SignatureVerificationComponent
  ]
})
export class NgxSignerModule {
  static forRoot(): ModuleWithProviders<NgxSignerModule> {
    return {
      ngModule: NgxSignerModule,
      providers: [
        {
          provide: SignerService,
          useFactory: SignerServiceFactory,
          deps: [
            AngularDataContext,
            HttpClient,
            ErrorService,
            TranslateService,
            ConfigurationService,
            BsModalService,
            TemplatePipe
          ],
        },
        SignActionsService
      ]
    };
  }
  constructor(private _translateService: TranslateService) {
    Object.keys(SIGNER_LOCALES).forEach((language) => {
      if (Object.prototype.hasOwnProperty.call(SIGNER_LOCALES, language)) {
        this._translateService.setTranslation(language, SIGNER_LOCALES[language], true);
      }
    });
  }
}
