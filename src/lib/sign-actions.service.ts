import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService, TemplatePipe, UserService } from '@universis/common';
import { SignerService, SignerServiceConfiguration } from './signer.service';

enum ActionStatusType {
  ActiveActionStatus = 'ActiveActionStatus',
  CompletedActionStatus = 'CompletedActionStatus',
  CancelledActionStatus = 'CancelledActionStatus',
  FailedActionStatus = 'FailedActionStatus',
  PotentialActionStatus = 'PotentialActionStatus'
}

enum QueryOptions {
  All = -1
}

export interface DocumentNumberSeriesItemSnapshot {
  id: number;
  documentCode?: string;
}

interface UserSnapshot {
  id: number;
}

export interface DocumentSignActionSnapshot {
  id: number;
  documentNumberSeriesItem: DocumentNumberSeriesItemSnapshot;
  actionStatus: {
    alternateName: string;
  };
}

interface SettingsConfiguration {
  signer?: SignerServiceConfiguration;
}

class InvalidActiveActionsError extends Error {
  constructor() {
    super('More than one or none active sign actions found for the specified document and owner');
  }
}

class MethodNotImplementedError extends Error {
  constructor() {
    super('Method not yet implemented');
  }
}

@Injectable()
export class SignActionsService {
  protected readonly user: UserSnapshot;
  protected readonly settings: SignerServiceConfiguration;

  constructor(
    protected readonly context: AngularDataContext,
    protected readonly userService: UserService,
    protected readonly signer: SignerService,
    protected readonly configuration: ConfigurationService,
    protected readonly template: TemplatePipe
  ) {
    // get current application user (only used for their id)
    this.user = this.userService.getUserSync();
    // get signer service configuration
    this.settings = (<SettingsConfiguration>this.configuration.settings).signer;
  }

  public async completeSignAction(document: DocumentNumberSeriesItemSnapshot, owner?: UserSnapshot, unsignedBlob?: Blob): Promise<any> {
    try {
      // set current application user as owner,if it is not provided
      if (owner == null) {
        owner = this.user;
      }
      const DocumentSignActions = this.context.model('DocumentSignActions');
      // get document sign actions for the specified document and owner
      const signActions: DocumentSignActionSnapshot[] = await DocumentSignActions.where('documentNumberSeriesItem')
        .equal(document.id)
        .and('owner')
        .equal(owner && owner.id)
        .select('id', 'actionStatus')
        .take(QueryOptions.All)
        .getItems();
      // if there are none
      if (!(Array.isArray(signActions) && signActions.length > 0)) {
        // exit
        return Promise.resolve();
      }
      // find the active action (there should be exactly one for this operation)
      const activeActions = signActions.filter((signAction) => {
        return signAction.actionStatus && signAction.actionStatus.alternateName === ActionStatusType.ActiveActionStatus;
      });
      if (activeActions.length !== 1) {
        throw new InvalidActiveActionsError();
      }
      const action = activeActions[0];
      // complete it
      const result = await DocumentSignActions.save({
        id: action.id,
        actionStatus: {
          alternateName: ActionStatusType.CompletedActionStatus
        },
        dateSigned: new Date(),
        signedBy: owner
      });
      // and exit
      return result;
    } catch (err) {
      // log error
      console.error(err);
      // if the original unsigned blob was provided
      if (unsignedBlob) {
        // rollback the replacement of the document
        const unsignedItem = {
          id: document.id,
          signed: false,
          published: false,
          datePublished: null
        };
        await this.signer.replaceDocument(unsignedItem, unsignedBlob);
      }
      // and throw the error
      throw err;
    }
  }

  public canSignDocument(document: DocumentNumberSeriesItemSnapshot): Promise<boolean> {
    return this.context.model(this.template.transform(this.canSignDocumentUrl(), document)).asQueryable().getItem();
  }

  protected canSignDocumentUrl() {
    return (this.settings && this.settings.documents && this.settings.documents.canSignDocumentUrl)
           || 'DocumentNumberSeriesItems/${id}/CanSignDocument';
  }

  public async getSignActions(owner?: UserSnapshot) {
    throw new MethodNotImplementedError();
  }

  public async getTitles() {
    throw new MethodNotImplementedError();
  }

  public async changeOwnership() {
    throw new MethodNotImplementedError();
  }
}
